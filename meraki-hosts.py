#!/usr/bin/env python3

import os
from meraki import meraki
import pprint as pp
from collections import Counter

API_KEY = os.environ['MERAKI_API_KEY']
DOMAN_NAME = os.environ['DOMAN_NAME']
clients = []
suppressprint = True
for org in meraki.myorgaccess(API_KEY, suppressprint=suppressprint):
    for network in meraki.getnetworklist(API_KEY, org['id'], suppressprint=suppressprint):
            for device in meraki.getnetworkdevices(API_KEY, network['id'], suppressprint=suppressprint):
                clients.extend([client for client in meraki.getclients(API_KEY, device['serial'], suppressprint=suppressprint) if client['id'] not in [client['id'] for client in clients]])

for client in clients:
    if client['description']:
        client['description'] = client['description'].replace("_","-")
        client['description'] = client['description'].replace(" ","-")
        if client['mdnsName']:
            host_line = "{ip} {description} {description}.{domain} {mdnsName}"
        else:
            host_line = "{ip} {description} {description}.{domain}"
        print(host_line.format(**client, domain=DOMAN_NAME))
    else:
        host_line = "{ip} {mac} {mac}.{domain}"
        print(host_line.format(ip=client['ip'], mac=client['mac'].replace(":","-"), domain=DOMAN_NAME))
