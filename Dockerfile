FROM debian:buster

ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -y --no-install-recommends python3-pip
RUN apt install -y --no-install-recommends   python3-setuptools
RUN pip3 install wheel
RUN pip3 install requests
RUN pip3 install meraki

COPY meraki-hosts.py /bin/

ENTRYPOINT ["/bin/meraki-hosts.py"]
