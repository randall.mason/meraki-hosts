# meraki-hosts

This pulls all of your hosts off of the dashboard so you can add it to an addn-hosts directive in dnsmasq.

Install the dependencies:
pip install meraki requests

Run it with two environment variables:
MERAKI_API_KEY and DOMAIN_NAME

It will print out a hosts file that you can then include in dnsmasq with addn-hosts.

```
docker build -t meraki-hosts .
docker run -e MERAKI_API_KEY=xxxxxxxxxxxxxxxxxxxxxx -e DOMAN_NAME=example.com meraki-hosts | docker exec -i pihole /usr/bin/tee /etc/pihole/lan.list
echo "addn-hosts=/etc/pihole/lan.list" | docker exec -i pihole /usr/bin/tee /etc/dnsmasq.d/02-lan.conf
```